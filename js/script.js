$(document).ready(function(){

	initPreset();
	initQuickCopy();

	update1From255();
	updateHexaFrom255();
	updateCMYKFromRGB();
	updateHSLFromRGB();
	updatePreview();

	$("input").change(checkAllValue);
	$("input").keyup(checkAllValue);

	$(".rgba255 input").change(updateAllFrom255);
	$(".rgba255 input").keyup(updateAllFrom255);

	$(".rgba1 input").change(updateAllFrom1);
	$(".rgba1 input").keyup(updateAllFrom1);

	$(".hexadecimal input").change(updateAllFromHexa);
	$(".hexadecimal input").keyup(updateAllFromHexa);

	$(".cmyk100 input").change(updateAllFromCMYK);
	$(".cmyk100 input").keyup(updateAllFromCMYK);

	$(".hsla100 input").change(updateAllFromHSLA);
	$(".hsla100 input").keyup(updateAllFromHSLA);

	$(".preset").click(function(){
		$("#hexa").val($(this).attr("data-preset"));
		update255FromHexa();
		update1From255();
		updateCMYKFromRGB();
		updateHSLFromRGB();
		updatePreview();
		$("body").scrollTop(100);
	});

	$(".regex").click(function(){
		window.currentRegex = $(this).attr("data-regex");
		$(".regex").removeClass('active');
		$(this).addClass('active');
		updateQuickCopy();
	});
});

function checkAllValue () {
	if ($('#r255').val() == '' || parseInt($('#r255').val()) < 0) {
		$('#r255').val(0);
	}
	if ($('#g255').val() == '' || parseInt($('#g255').val()) < 0) {
		$('#g255').val(0);
	}
	if ($('#b255').val() == '' || parseInt($('#b255').val()) < 0) {
		$('#b255').val(0);
	}
	if ($('#a255').val() == '' || parseInt($('#a255').val()) < 0) {
		$('#a255').val(0);
	}

	if ($('#r1').val() == '' || parseFloat($('#r1').val()) < 0) {
		$('#r1').val(0);
	}
	if ($('#g1').val() == '' || parseFloat($('#g1').val()) < 0) {
		$('#g1').val(0);
	}
	if ($('#b1').val() == '' || parseFloat($('#b1').val()) < 0) {
		$('#b1').val(0);
	}
	if ($('#a1').val() == '' || parseFloat($('#a1').val()) < 0) {
		$('#a1').val(0);
	}

	if ($('#c100').val() == '' || parseInt($('#c100').val()) < 0) {
		$('#c100').val(0);
	}
	if ($('#m100').val() == '' || parseInt($('#m100').val()) < 0) {
		$('#m100').val(0);
	}
	if ($('#y100').val() == '' || parseInt($('#y100').val()) < 0) {
		$('#y100').val(0);
	}
	if ($('#k100').val() == '' || parseInt($('#k100').val()) < 0) {
		$('#k100').val(0);
	}

	if ($('#h360').val() == '' || parseInt($('#h360').val()) < 0) {
		var h = parseInt($('#h360').val());
		while (h <= 0) {
			h = h + 360;
			console.log(h);
		}
		$('#h360').val(h);
	}
	if ($('#s100').val() == '' || parseInt($('#s100').val()) < 0) {
		$('#s100').val(0);
	}
	if ($('#l100').val() == '' || parseInt($('#l100').val()) < 0) {
		$('#l100').val(0);
	}
	if ($('#a100').val() == '' || parseInt($('#a100').val()) < 0) {
		$('#a100').val(0);
	}

	/*if ($('#hexa').val() == '') {
		updateHexaFrom255();
	}*/

	if (parseInt($('#r255').val()) > 255) {
		$('#r255').val(255);
	}
	if (parseInt($('#g255').val()) > 255) {
		$('#g255').val(255);
	}
	if (parseInt($('#b255').val()) > 255) {
		$('#b255').val(255);
	}
	if (parseInt($('#a255').val()) > 255) {
		$('#a255').val(255);
	}

	if (parseFloat($('#r1').val()) > 1) {
		$('#r1').val(1);
	}
	if (parseFloat($('#g1').val()) > 1) {
		$('#g1').val(1);
	}
	if (parseFloat($('#b1').val()) > 1) {
		$('#b1').val(1);
	}
	if (parseFloat($('#a1').val()) > 1) {
		$('#a1').val(1);
	}

	if (parseInt($('#c100').val()) > 100) {
		$('#c100').val(100);
	}
	if (parseInt($('#m100').val()) > 100) {
		$('#m100').val(100);
	}
	if (parseInt($('#y100').val()) > 100) {
		$('#y100').val(100);
	}
	if (parseInt($('#k100').val()) > 100) {
		$('#k100').val(100);
	}

	if (parseInt($('#h360').val()) > 360) {
		var h = parseInt($('#h360').val());
		h = h % 360;
		$('#h360').val(h);
	}
	if (parseInt($('#s100').val()) > 100) {
		$('#s100').val(100);
	}
	if (parseInt($('#l100').val()) > 100) {
		$('#l100').val(100);
	}
	if (parseInt($('#a100').val()) > 100) {
		$('#a100').val(100);
	}
}

function convert255To1 (value) {
	return Math.round((value / 255) * 1000) / 1000;
}
function convert1To255 (value) {
	return Math.round(value * 255);
}
function convert255ToFF (value) {
	var hex = parseInt(value).toString(16);
	if (hex.length == 1)
		hex = "0"+hex;
	return hex;
}
function convertFFTo255 (value) {
	return parseInt(value, 16);
}

function update1From255 () {
	$("#r1").val(convert255To1($("#r255").val()));
	$("#g1").val(convert255To1($("#g255").val()));
	$("#b1").val(convert255To1($("#b255").val()));
	$("#a1").val(convert255To1($("#a255").val()));
}
function update255From1 () {
	$("#r255").val(convert1To255($("#r1").val()));
	$("#g255").val(convert1To255($("#g1").val()));
	$("#b255").val(convert1To255($("#b1").val()));
	$("#a255").val(convert1To255($("#a1").val()));
}
function update255FromHexa () {
	var hex = $("#hexa").val();
	var hexR, hexG, hexB, hexA;
	if (hex.length != 3 && hex.length != 4 && hex.length != 6 && hex.length != 8){
		return;
	} else if (hex.length <= 4) {
		hexR = hex.substr(0,1) + hex.substr(0,1);
		hexG = hex.substr(1,1) + hex.substr(1,1);
		hexB = hex.substr(2,1) + hex.substr(2,1);
		if (hex.length > 3)
			hexA = hex.substr(3,1) + hex.substr(3,1);
		else
			hexA = "FF";
	} else {
		hexR = hex.substr(0,2);
		hexG = hex.substr(2,2);
		hexB = hex.substr(4,2);
		if (hex.length > 6)
			hexA = hex.substr(6,2);
		else
			hexA = "FF";
	}
	$("#r255").val(convertFFTo255(hexR));
	$("#g255").val(convertFFTo255(hexG));
	$("#b255").val(convertFFTo255(hexB));
	$("#a255").val(convertFFTo255(hexA));
}
function updateHexaFrom255 () {
	var r255 = $("#r255").val();
	var g255 = $("#g255").val();
	var b255 = $("#b255").val();
	var a255 = $("#a255").val();

	$("#hexa").val(convert255ToFF(r255)+convert255ToFF(g255)+convert255ToFF(b255)+convert255ToFF(a255));
}
function updateRGBFromCMYK () {
	var c = ($("#c100").val()) / 100;
	var m = ($("#m100").val()) / 100;
	var y = ($("#y100").val()) / 100;
	var k = ($("#k100").val()) / 100;

	var r = 255 * (1-c) * (1-k);
	var g = 255 * (1-m) * (1-k);
	var b = 255 * (1-y) * (1-k);

	$("#r255").val(parseInt(r));
	$("#g255").val(parseInt(g));
	$("#b255").val(parseInt(b));
	$("#a255").val(255);
}
function updateCMYKFromRGB () {
	var a = $("#a1").val();
	var r = ((1-a) * 255 + a * $("#r255").val()) / 255;
	var g = ((1-a) * 255 + a * $("#g255").val()) / 255;
	var b = ((1-a) * 255 + a * $("#b255").val()) / 255;

	var k = 1-max(r,g,b);
	if (k < 1) {
		var c = (1-r-k) / (1-k);
		var m = (1-g-k) / (1-k);
		var y = (1-b-k) / (1-k);
	} else {
		var c = 0;
		var m = 0;
		var y = 0;
	}

	k = parseInt(k*100);
	c = parseInt(c*100);
	m = parseInt(m*100);
	y = parseInt(y*100);

	$("#k100").val(k);
	$("#c100").val(c);
	$("#m100").val(m);
	$("#y100").val(y);
}
function updateRGBFromHSL () {
	var h = parseFloat($("#h360").val()) / 360;
	var s = parseFloat($("#s100").val()) / 100;
	var l = parseFloat($("#l100").val()) / 100;
	var a = parseFloat($("#a100").val());

	var r, g, b;

    if (s == 0) {
        r = g = b = l; // achromatic
    } else {
        function hue2rgb(p, q, t) {
            if(t < 0) t += 1;
            if(t > 1) t -= 1;
            if(t < 1/6) return p + (q - p) * 6 * t;
            if(t < 1/2) return q;
            if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
            return p;
        }

        var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
        var p = 2 * l - q;
        r = hue2rgb(p, q, h + 1/3);
        g = hue2rgb(p, q, h);
        b = hue2rgb(p, q, h - 1/3);
    }

    $("#r255").val(parseInt(Math.round(r*255)));
	$("#g255").val(parseInt(Math.round(g*255)));
	$("#b255").val(parseInt(Math.round(b*255)));
	$("#a255").val(parseInt(Math.round(a*255)));
}
function updateHSLFromRGB () {
	var r = parseFloat($("#r1").val());
	var g = parseFloat($("#g1").val());
	var b = parseFloat($("#b1").val());
	var a = parseFloat($("#a1").val());

	var max = Math.max(r, g, b)
	var min = Math.min(r, g, b);
	var h, s, l = (max + min) / 2;

	if(max == min){
        h = s = 0; // achromatic
    } else {
        var d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
        switch(max){
            case r:
            	h = (g - b) / d + (g < b ? 6 : 0);
            	break;
            case g:
            	h = (b - r) / d + 2;
            	break;
            case b:
            	h = (r - g) / d + 4;
            	break;
        }
        h /= 6;
    }

	$("#h360").val(parseInt(Math.round(h*360)));
	$("#s100").val(parseInt(Math.round(s*100)));
	$("#l100").val(parseInt(Math.round(l*100)));
	$("#a100").val(a);
}

function updatePreview () {
	var value = "rgba("+$("#r255").val()+","+$("#g255").val()+","+$("#b255").val()+","+$("#a1").val()+")";
	$(".color_preview #dynamic_color").css("background-color",value);
	updateQuickCopy();
}

window.currentRegex = '';
function updateQuickCopy () {
	var strResult = window.currentRegex;

	strResult = strResult.replace(/@r255/ig, $("#r255").val());
	strResult = strResult.replace(/@g255/ig, $("#g255").val());
	strResult = strResult.replace(/@b255/ig, $("#b255").val());
	strResult = strResult.replace(/@a255/ig, $("#a255").val());

	strResult = strResult.replace(/@r1/ig, $("#r1").val());
	strResult = strResult.replace(/@g1/ig, $("#g1").val());
	strResult = strResult.replace(/@b1/ig, $("#b1").val());
	strResult = strResult.replace(/@a1/ig, $("#a1").val());

	strResult = strResult.replace(/@rHex/ig, convert255ToFF($("#r255").val()));
	strResult = strResult.replace(/@gHex/ig, convert255ToFF($("#g255").val()));
	strResult = strResult.replace(/@bHex/ig, convert255ToFF($("#b255").val()));
	strResult = strResult.replace(/@aHex/ig, convert255ToFF($("#a255").val()));
	strResult = strResult.replace(/@rgbHex/ig, convert255ToFF($("#r255").val())+convert255ToFF($("#g255").val())+convert255ToFF($("#b255").val()));
	strResult = strResult.replace(/@rgbaHex/ig, convert255ToFF($("#r255").val())+convert255ToFF($("#g255").val())+convert255ToFF($("#b255").val())+convert255ToFF($("#a255").val()));
	strResult = strResult.replace(/@argbHex/ig, convert255ToFF($("#a255").val())+convert255ToFF($("#r255").val())+convert255ToFF($("#g255").val())+convert255ToFF($("#b255").val()));

	strResult = strResult.replace(/@c100/ig, $("#c100").val());
	strResult = strResult.replace(/@m100/ig, $("#m100").val());
	strResult = strResult.replace(/@y100/ig, $("#y100").val());
	strResult = strResult.replace(/@k100/ig, $("#k100").val());

	strResult = strResult.replace(/@h360/ig, $("#h360").val());
	strResult = strResult.replace(/@s100/ig, $("#s100").val());
	strResult = strResult.replace(/@l100/ig, $("#l100").val());
	strResult = strResult.replace(/@a1/ig, $("#a100").val()); /* obsolete */

	$('#quick_copy').html(strResult);
}

function updateAllFrom255 () {
	update1From255();
	updateHexaFrom255();
	updateCMYKFromRGB();
	updateHSLFromRGB();

	updatePreview();
}
function updateAllFrom1 () {
	update255From1();
	updateHexaFrom255();
	updateCMYKFromRGB();
	updateHSLFromRGB();

	updatePreview();
}
function updateAllFromHexa () {
	update255FromHexa();
	update1From255();
	updateCMYKFromRGB();
	updateHSLFromRGB();

	updatePreview();
}
function updateAllFromCMYK () {
	updateRGBFromCMYK();
	update1From255();
	updateHexaFrom255();
	updateHSLFromRGB();

	updatePreview();
}
function updateAllFromHSLA () {
	updateRGBFromHSL();
	update1From255();
	updateHexaFrom255();
	updateCMYKFromRGB();

	updatePreview();
}

function initQuickCopy () {
	for (i = 0; i < quickCopy.length; i++) {
		var data_regex = quickCopy[i].regex;
		var data_tracker = quickCopy[i].language;
		$('#language_bar').append('<span class="regex" data-regex="'+data_regex+'" data-tracker="'+data_tracker+'">'+quickCopy[i].language+'</span>');
	}
	//$('#language_bar').append('<span id="custom_regex" class="regex">Custom</span>');

	$('#language_bar span:first-child').addClass('active');
	window.currentRegex = $('#language_bar span:first-child').attr("data-regex");
}

function initPreset () {
	for (i = 0; i < presets.length; i++) {
		$('#presets_zone > div').append('<div id="group'+i+'"></div>');
		$('#group'+i).append('<h3><a href="'+presets[i].lien+'" target="_blank">'+presets[i].titre+' <i data-icon="'+presets[i].icon+'"></i></a></h3>')
		$('#group'+i).append('<div class="preset_list"></div>');
		for (j = 0; j < presets[i].colors.length; j++) {
			var data_preset = presets[i].colors[j].hex;
			var data_tracker = presets[i].titre +'-'+ presets[i].colors[j].hex +'-'+ presets[i].colors[j].name;
			$('#group'+i+' .preset_list').append('<div class="preset" data-preset="'+data_preset+'" data-tracker="'+data_tracker+'"></div>');
			$('#group'+i+' .preset_list [data-preset="'+data_preset+'"]').append('<div class="prev"><div style="background-color:#'+presets[i].colors[j].hex+'"></div></div>');
			$('#group'+i+' .preset_list [data-preset="'+data_preset+'"]').append(' <span>    #'+presets[i].colors[j].hex+'   </span>  ');
			if (presets[i].colors[j].name != "") {
				$('#group'+i+' .preset_list [data-preset="'+data_preset+'"]').append(' <span class="preset-name">'+presets[i].colors[j].name+'</span>  ');
			}
		}
	}
}




function min () {
	var min = arguments[0];
	for (i=1; i<arguments.length; i++) {
		if (arguments[i] < min) {
			min = arguments[i];
		}
	}
	return min;
}

function max () {
	var max = arguments[0];
	for (i=1; i<arguments.length; i++) {
		if (arguments[i] > max) {
			max = arguments[i];
		}
	}
	return max;
}