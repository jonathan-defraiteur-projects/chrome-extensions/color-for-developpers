chrome.app.runtime.onLaunched.addListener(function() {
    chrome.app.window.create('color_app.html', {
        'outerBounds': {
            /*'width': 1024,
            'height': 700*/
            'width': 1280,
            'height': 800
        },
        'frame': {
            'type': 'chrome',
            'color': '#CCCCCC',
            'activeColor': '#CCCCCC',
            'inactiveColor': '#888888'
        }
    });
});